package m360;

public class Heizung {
    private int temperature;
    private int min;
    private int max;
    private int increment;

    public Heizung(){}

    public Heizung(float temperature, int increment){
        this.temperature = (int) temperature;
        this.increment = increment;
        this.min = this.temperature - increment;
        this.max = this.temperature + increment;
    }

    public Heizung(int min, int max){
        this.temperature = 15;
        this.min = min;
        this.max = max;}

    public void changeTemperature(int num){
        if(getTemperature() + num > getMin() && getTemperature() + num < getMax()) {
            setTemperature(getTemperature() + num);
            System.out.println("Temperature has been changed to : " + getTemperature());
        } else {
            System.out.println("New temperature is out of limits!!!!!");
        }
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getIncrement() {
        return increment;
    }

    public void setIncrement(int increment) {
        this.increment = increment;
    }
}
