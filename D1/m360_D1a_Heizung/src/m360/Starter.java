package m360;

public class Starter {
    public static void main(String[] args) {
        Heizung heizungA = new Heizung(13f, 2);
        Heizung heizungB = new Heizung(0, 100);
        Heizung heizungC = new Heizung();
        heizungC.setTemperature(25);
        heizungC.setIncrement(30);
        heizungC.setMin(heizungC.getTemperature() - heizungC.getIncrement());
        heizungC.setMax(heizungC.getTemperature() + heizungC.getIncrement());


        heizungA.changeTemperature(1);
        heizungA.changeTemperature(20);
        heizungB.changeTemperature(30);
        heizungB.changeTemperature(120);
        heizungC.changeTemperature(10);
        heizungC.changeTemperature(40);
    }
}
