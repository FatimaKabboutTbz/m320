package tbz;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        SchoolClass schoolClass = new SchoolClass();
        int state;

        System.out.println("SchoolClass Manager");
        System.out.println("*********************");

        do{
            System.out.println("Choose an option: ");
            System.out.println("[0] Add Student | [1]Get SchoolClass Average | [2] exit");
            state = input.nextInt();
            switch (state){
                case 0:
                    String studentname;
                    float points;
                    System.out.println("Enter student name: ");
                    input.nextLine();
                    studentname = input.nextLine();
                    System.out.println("Enter points  from 0 - 20 for the test");
                    points = input.nextFloat();
                    Student s = new Student(studentname);
                    s.addTest(new Test(20f, points));
                    schoolClass.addStudent(s);
                    break;
                case 1:
                    System.out.println("The average grade for this class is: " + schoolClass.getAverage());
                    break;
                case 2: System.exit(0);
                default:
                    System.out.println("Enter correct option");
            }
            System.out.println("*******************");
        } while (state != 2);


        System.out.println("Thanks for using the program!");
    }
}