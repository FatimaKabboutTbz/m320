package tbz;

import java.util.ArrayList;

public class SchoolClass {
    private ArrayList<Student> students = new ArrayList<>();
    public float getAverage(){
        float totalGrades = 0f;
        for (Student s: students) {
            totalGrades += s.getNotenschnitt();
            System.out.println("Average grade of " + s.getName() + " = " + s.getNotenschnitt());
        }
        return (totalGrades / students.size());
    }
    public void addStudent(Student s){
        if (students.size() < 20){
            students.add(s);
        } else {
            System.out.println("Limit of 20 students has been reached!");
        }
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }
}
