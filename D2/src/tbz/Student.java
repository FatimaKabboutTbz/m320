package tbz;

import java.util.ArrayList;

public class Student {
    private String name;
    private ArrayList<Test> tests = new ArrayList<>();
    public Student(String name){
        this.name = name;
    }

    public float getNotenschnitt(){
        float allGrades = 0f;
        for(int i = 0; i < getTests().size(); i++){
            allGrades += getTests().get(i).getGrade();
        }

        return (allGrades / getTests().size());
    }
    public void addTest(Test t){
        tests.add(t);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Test> getTests() {
        return tests;
    }

    public void setTests(ArrayList<Test> tests) {
        this.tests = tests;
    }
}
