package tbz;

public class Test {
    private float totalPoints;

    private float studentPoints;

    private float grade;

    public Test(float totalPoints, float studentPoints) {
        this.totalPoints = totalPoints;
        this.studentPoints = studentPoints;
        calculateGrade();
    }

    public float calculateGrade(){
        float newgrade = getStudentPoints() * 5f / getTotalPoints() + 1f;
        System.out.println(newgrade);
        setGrade(newgrade);
        return getGrade();
    }

    public float getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public float getStudentPoints() {
        return studentPoints;
    }

    public void setStudentPoints(int studentPoints) {
        this.studentPoints = studentPoints;
    }

    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        this.grade = grade;
    }
}
