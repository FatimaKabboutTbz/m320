package tbz;

import java.util.*;

public class FriendNetwork {
    private Map<String, List<String>> network;

    public FriendNetwork() {
        this.network = new HashMap<>();
    }

    public void addFriend(String person, List<String> friends) {
        network.put(person, friends);
    }

    public void printNetwork() {
        for (Map.Entry<String, List<String>> entry : network.entrySet()) {
            System.out.print(entry.getKey() + " -> ");
            System.out.println(entry.getValue());
        }
    }
}
