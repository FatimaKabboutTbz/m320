package tbz;

import java.util.Arrays;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        FriendNetwork friendNetwork = new FriendNetwork();

        friendNetwork.addFriend("Alice", Arrays.asList("Bob", "Charlie"));
        friendNetwork.addFriend("Bob", Arrays.asList("Alice", "David"));
        friendNetwork.addFriend("Charlie", Arrays.asList("Alice", "Eve"));
        friendNetwork.addFriend("David", Collections.singletonList("Bob"));
        friendNetwork.addFriend("Eve", Collections.singletonList("Charlie"));

        System.out.println("Friend Network:");
        friendNetwork.printNetwork();
    }
}