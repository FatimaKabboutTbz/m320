# M320

### D3 - Description

V3 - Interfaces anwenden

This application simulates a simple stock portfolio management system. It consists of three main components: the Stock exchange interface, implementations for the New York Stock Exchange and Zurich Stock Exchange, and a Portfolio class representing a collection of stocks. 
These stocks can be created and managed by the user, ultimately displaying their Stocks in currency on the console.