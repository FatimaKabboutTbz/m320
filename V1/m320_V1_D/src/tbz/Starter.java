package tbz;

import java.util.ArrayList;
import java.util.List;

public class Starter {
    public static void main(String[] args) {
        Direktor direktor = new Direktor("tbz.Direktor Hans");
        Sekretaerin sekretaerin = new Sekretaerin("tbz.Sekretaerin Anna");
        Sozialarbeiter sozialarbeiter = new Sozialarbeiter("tbz.Sozialarbeiter Max");
        Lehrer lehrer = new Lehrer("tbz.Lehrer Schmidt");
        Schueler schueler = new Schueler("tbz.Schueler Kevin");

        List<Person> list = new ArrayList<>();
        list.add(direktor);
        list.add(sekretaerin);
        list.add(sozialarbeiter);
        list.add(lehrer);
        list.add(schueler);

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getName());
        }
    }
}
