package tbz;


/**
 * @Author: Fatima
 */
public class Angreifer extends Spieler{

    public Angreifer(String name) {
        super(name);
    }

    /**
     * This Method prints out that the Angreifer has completed his jog training.
     */
    public void jogTraining(){
        System.out.println("Angreifer absolviert das Jog Training.\n");
    }

    /**
     * This method ovrrides the spielen method from the Spieler class and prints something new out.
     */
    @Override
    public void spielen(){
        System.out.println("Der Angreifer spielt.");
    }
}
