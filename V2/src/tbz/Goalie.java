package tbz;

/**
 * @Author Jessica
 */
public class Goalie extends Spieler{

    public Goalie(String name) {
        super(name);
    }

    /**
     * This method prints out the height of the Goalie.
     * @param groesse
     */
    public void koerperGroesse(double groesse){
        System.out.println("Das ist die Körpergrösse: " + groesse + "\n");
    }

    /**
     * This method ovrrides the spielen method from the Spieler class and prints something new out.
     */
    @Override
    public void spielen(){
        System.out.println("Der Goalie spielt.");
    }
}
