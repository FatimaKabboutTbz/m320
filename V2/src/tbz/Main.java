package tbz;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Goalie goalie = new Goalie("Jim");
        Angreifer angreifer = new Angreifer("Bill");
        Verteidiger verteidiger = new Verteidiger("Zach");
        Mannschaft mannschaft = new Mannschaft(goalie, List.of(angreifer), List.of(verteidiger));

    }
}