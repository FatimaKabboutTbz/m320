package tbz;

import java.util.List;

/**
 * In this Class we create a new Mannschaft with one Goalie, a list of Angreier and Verteidiger.
 * @Author Fatima
 */
public class Mannschaft {
    private Goalie goalie;
    private List<Angreifer> angreifers;
    private List<Verteidiger> verteidigers;

    public Mannschaft(Goalie goalie, List<Angreifer> angreifers, List<Verteidiger> verteidigers) {
        this.goalie = goalie;
        this.angreifers = angreifers;
        this.verteidigers = verteidigers;

        goalie.spielen();
        goalie.zeigeName();
        goalie.koerperGroesse(1.80d);

        for(Angreifer angreifer : angreifers){
            angreifer.spielen();
            angreifer.zeigeName();
            angreifer.jogTraining();
        }

        for(Verteidiger verteidiger: verteidigers){
            verteidiger.spielen();
            verteidiger.zeigeName();
        }
    }

    /**
     * This method adds a new Angrifer to the Angreifer list.
     * @param angreifer
     */
    public void addNewAngreifer(Angreifer angreifer){
        angreifer.zeigeName();
        angreifer.jogTraining();
        angreifer.spielen();
        getAngreifers().add(angreifer);
    }

    /**
     * This method adds a new verteidiger to the Verteidiger list.
     * @param verteidiger
     */
    public void addNewVerteidiger(Verteidiger verteidiger){
        verteidiger.zeigeName();
        verteidiger.spielen();
        getVerteidigers().add(verteidiger);
    }

    /**
     * This method sets a new Goalie and calls three different methods.
     * @param goalie
     */
    public void setNewGoalie(Goalie goalie){
        goalie.zeigeName();
        goalie.koerperGroesse(1.80d);
        goalie.spielen();
        setGoalie(goalie);
    }

    public Goalie getGoalie() {
        return goalie;
    }

    public void setGoalie(Goalie goalie) {
        this.goalie = goalie;
    }

    public List<Angreifer> getAngreifers() {
        return angreifers;
    }

    public void setAngreifers(List<Angreifer> angreifers) {
        this.angreifers = angreifers;
    }

    public List<Verteidiger> getVerteidigers() {
        return verteidigers;
    }

    public void setVerteidigers(List<Verteidiger> verteidigers) {
        this.verteidigers = verteidigers;
    }
}
