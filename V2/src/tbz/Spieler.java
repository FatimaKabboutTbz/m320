package tbz;

/**
 * @Author Fatima
 */
public class Spieler {
    private String name;

    public Spieler(String name) {
        this.name = name;
    }

    /**
     * This method prints out the names of the players.
     */
    public void zeigeName(){
        System.out.println("Spieler name: " + getName());
    }

    /**
     * This method prints out that the game has started.
     */
    public void spielen(){
        System.out.println("Das spiel fängt an.");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
