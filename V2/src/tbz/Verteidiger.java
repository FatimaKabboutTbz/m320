package tbz;

/**
 * @Author Jessica
 */
public class Verteidiger extends Spieler{

    public Verteidiger(String name) {
        super(name);
    }

    /**
     * This method overrides the spielen method from the Spieler class and prints something new out.
     */
    @Override
    public void spielen(){
        System.out.println("Der Verteidiger spielt.");
    }
}
