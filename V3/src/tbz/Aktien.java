package tbz;

public class Aktien {
    private String symbol;
    private int quantity;

    public Aktien(String symbol, int quantity) {
        this.symbol = symbol;
        this.quantity = quantity;
    }

    public String getSymbol() {
        return symbol;
    }

    public int getQuantity() {
        return quantity;
    }
}
