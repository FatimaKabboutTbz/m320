package tbz;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        StockExchange zurichStockExchange = new ZurichStockExchange();
        StockExchange newYorkStockExchange = new NyStockExchange();
        Portfolio zurichPortfolio = new Portfolio(zurichStockExchange);
        Portfolio newYorkPortfolio = new Portfolio(newYorkStockExchange);
        boolean run = true;
        int userInput;

        System.out.println("Welcome to Stock Exchange.");
        Scanner input = new Scanner(System.in);
        do {
            System.out.println("Choose an option:");
            System.out.println("[0] Add to NY Portfolio, [1] Add to ZH Portfolio, [2] Calculate NY Value, [3] Calculate ZH Value, [4] Exit");
            try {
                userInput = input.nextInt();
                input.nextLine();
                switch (userInput) {
                    case 0:
                        newYorkPortfolio.addStock(createStock(input));
                        System.out.println("Stock added to NY Portfolio");
                        break;
                    case 1:
                        zurichPortfolio.addStock(createStock(input));
                        System.out.println("Stock added to ZH Portfolio");
                        break;
                    case 2:
                        double newYorkPortfolioValue = newYorkPortfolio.calculatePortfolioValue();
                        System.out.println("NY Portfolio Value: $" + newYorkPortfolioValue);
                        break;
                    case 3:
                        double zurichPortfolioValue = zurichPortfolio.calculatePortfolioValue();
                        System.out.println("Zurich Portfolio Value: " + zurichPortfolioValue + " CHF");
                        break;
                    case 4:
                        System.out.println("Goodbye!");
                        run = false;
                        break;
                    default:
                        System.out.println("Please enter one of the options.");
                }
            } catch (NoSuchElementException e) {
                System.out.println("Please input a valid NUMBER");
                input.nextLine();
            }
        } while (run);
        input.close();
    }

    public static Aktien createStock(Scanner scanner) {
        String stockSymbol = "";
        int quantity = 0;

        System.out.println("Enter the Stock Symbol: ");
        stockSymbol = scanner.nextLine();
        try {
            System.out.println("Enter the quantity: ");
            quantity = scanner.nextInt();
        } catch (InputMismatchException E) {
            System.out.println("Please enter valid numbers!");
            scanner.nextLine();
        }

        return new Aktien(stockSymbol, quantity);
    }
}
