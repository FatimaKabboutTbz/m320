package tbz;

public class NyStockExchange implements StockExchange{
    @Override
    public double getPrice(String aktienSymbol) {
        if ("Microsoft".equals(aktienSymbol)) {
            return 100;
        }
        return 10.0;
    }
}
