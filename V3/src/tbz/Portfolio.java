package tbz;

import java.util.ArrayList;
import java.util.Collection;

public class Portfolio {
    private StockExchange aktienMarkt;
    private Collection<Aktien> aktien;

    public Portfolio(StockExchange aktienMarkt) {
        this.aktienMarkt = aktienMarkt;
        this.aktien = new ArrayList<>();
    }

    public void addStock(Aktien aktie) {
        aktien.add(aktie);
    }

    public double calculatePortfolioValue() {
        double totalValue = 0.0;
        for (Aktien aktie : aktien) {
            double stockPrice = aktienMarkt.getPrice(aktie.getSymbol());
            totalValue += stockPrice * aktie.getQuantity();
        }
        return totalValue;
    }
}
