package tbz;

public interface StockExchange {
    double getPrice(String stockSymbol);
}
