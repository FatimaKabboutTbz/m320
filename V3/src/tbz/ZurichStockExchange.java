package tbz;

public class ZurichStockExchange implements StockExchange{
    @Override
    public double getPrice(String aktienSymbol) {
        if ("Microsoft".equals(aktienSymbol)) {
            return 120;
        }
        return 12.0;
    }
}
